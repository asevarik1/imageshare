import { StatusBar } from 'expo-status-bar';
import * as React from 'react';
import { StyleSheet, View, ImageBackground,Image } from 'react-native';
import {Appbar, Menu,Button,Provider} from 'react-native-paper'; 
import * as Sharing from 'expo-sharing';
import { Asset } from 'expo-asset';
function App() {
  const imagedata={skyblue:require('./assets/blue.jpg'),pink:require('./assets/pink.jpg'),orange:require('./assets/orange.jpg'),red:require('./assets/red.jpg'),black:require('./assets/black.png')}
  const [visible, setVisible] = React.useState(false);
  const [color,setcolor]=React.useState(imagedata.pink);
  const openMenu = () => setVisible(true);
  const closeMenu = () => setVisible(false);
  let openShareDialogAsync = async () => {
    if (!(await Sharing.isAvailableAsync())) {
      alert(`Uh oh, sharing isn't available on your platform`);
      return;
    }
    const [{ localUri }] = await Asset.loadAsync(color);
    await Sharing.shareAsync(localUri);
  }; 
  return (
    <Provider>
    <View style={{...styles.container,backgroundColor:'transparent'}}>
    <ImageBackground source={color} style={{width:'100%',height:'100%'}}>
       <Appbar.Header style={{...styles.header,backgroundColor:'transparent'}}>
      <Appbar.Content title="ImageSender" style={{...styles.header,backgroundColor:'transparent',}}/>
    </Appbar.Header>
    
    <Menu visible={visible}
          onDismiss={closeMenu}
          anchor={<Button onPress={openMenu}>Choose backgroundColor</Button>}>
          <Menu.Item onPress={() => {setcolor(imagedata.pink)}} title="pink" />
          <Menu.Item onPress={() => {setcolor(imagedata.skyblue)}} title="sky Blue" />
          <Menu.Item onPress={() => {setcolor(imagedata.orange)}} title="Orange" />
          <Menu.Item onPress={() => {setcolor(imagedata.red)}} title="red" />
          <Menu.Item onPress={() => {setcolor(imagedata.black)}} title="black" />
        </Menu>
      <Button onPress={openShareDialogAsync}>Share</Button>    
        <StatusBar style="auto" />
        </ImageBackground>
    </View>
    </Provider>
  );
}
export default App
const styles = StyleSheet.create({
  container: {
    flex:1,
    },
  text:{
    margin:30,
    backgroundColor:'transparent',
    marginTop:300
  },
  header:{
    alignItems:'center',
    fontWeight:'bold'
  }
});
